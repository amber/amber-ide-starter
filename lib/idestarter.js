define(["tingle", "mousetrap", "css!tingle"], function (tingle, mouseTrap) {
    var modal = new tingle.modal({
        footer: true,
        stickyFooter: false,
        closeMethods: ['overlay', 'escape'],
        onOpen: function() {
            mouseTrap.unbind(exports.keystroke);
        },
        onClose: function() {
            mouseTrap.bind(exports.keystroke, openMe);
        }
    });

    modal.addFooterBtn('legacy IDE', 'tingle-btn', function () {
        require('amber/helpers').globals.Browser._open();
        modal.close();
    });

    modal.addFooterBtn('Helios IDE', 'tingle-btn', function () {
        require('amber/helpers').popupHelios();
        modal.close();
    });

    var exports = {
        html: "<p><em>Esc</em> to escape; <em>Shift Shift Ctrl Shift</em> to show again</p>",
        keystroke: "shift shift ctrl shift",
        start: function () {
            setTimeout(openMe);
        }
    };

    function openMe() {
        modal.setContent(exports.html);
        modal.open();
    }

    return exports;
});
/* DO NOT EDIT! This file is generated. */

var require;
if (!require) require = {config: function (x) {require = x;}};
require.config({
  "paths": {
    "tingle": "node_modules/tingle.js/dist/tingle",
    "require-css": "node_modules/require-css",
    "mousetrap": "node_modules/mousetrap/mousetrap",
    "amber-ide-starter-dialog": "lib/idestarter"
  },
  "map": {
    "*": {
      "css": "require-css/css"
    }
  }
});